import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthorizedGuard } from '@shared/guards/authorized.guard';

const routes: Routes = [
  {
    path: 'dashboard',
    title: 'NASA Dashboard',
    data: { name: 'Dashboard' },
    canActivate: [ AuthorizedGuard ],
    canMatch: [ AuthorizedGuard ],
    canActivateChild: [ AuthorizedGuard ],
    loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'apod',
    title: 'NASA Dashboard - APOD',
    data: { name: 'Pictures' },
    canActivate: [ AuthorizedGuard ],
    canMatch: [ AuthorizedGuard ],
    canActivateChild: [ AuthorizedGuard ],
    loadChildren: () => import('./modules/apod/apod.module').then(m => m.ApodModule)
  },
  {
    path: 'neo',
    title: 'NASA Dashboard - NEO',
    data: { name: 'Near Earth Objects' },
    canActivate: [ AuthorizedGuard ],
    canMatch: [ AuthorizedGuard ],
    canActivateChild: [ AuthorizedGuard ],
    loadChildren: () => import('./modules/neo/neo.module').then(m => m.NeoModule)
  },
  { 
    path: 'auth',
    title: 'NASA Dashboard - Authorization',
    loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'dashboard'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
