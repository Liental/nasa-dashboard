import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';

import { ApodRoutingModule } from './apod-routing.module';
import { ApodComponent } from './components/apod/apod.component';

@NgModule({
  declarations: [
    ApodComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ApodRoutingModule
  ]
})
export class ApodModule { }
