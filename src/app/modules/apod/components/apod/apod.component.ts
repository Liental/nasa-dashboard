import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-apod',
  templateUrl: './apod.component.html',
  styleUrls: ['./apod.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApodComponent {

}
