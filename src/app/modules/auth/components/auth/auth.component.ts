import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthComponent {
  /** Form group used to validate the API key */
  protected formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private cookieService: CookieService,
    private router: Router
  ) {}

  /** Is the API key filled in */
  protected get isValid(): boolean {
    return !!this.formGroup.get('apiKey')?.valid;
  }

  ngOnInit(): void {
    this.cookieService.delete('apiKey');
    
    this.formGroup = this.formBuilder.group({
      apiKey: [ '', [ Validators.required, Validators.minLength(1) ] ]
    });
  }

  /** Set the authorization cookie and redirect to dashboard */
  protected onSubmit(): void {
    if (!this.isValid) {
      return;
    }

    this.cookieService.set('apiKey', this.formGroup.get('apiKey')!.value);
    this.router.navigateByUrl('dashboard');
  }
}
