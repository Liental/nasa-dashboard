import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent {
  /** Is APOD module loaded */
  protected isApodReady = false;

  /** Is NEO module loaded */
  protected isNeoReady = false;

  /** Hide the loader if both modules are loaded */
  protected get isReady(): boolean {
    return this.isApodReady && this.isNeoReady;
  }
}
