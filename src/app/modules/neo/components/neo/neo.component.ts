import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-neo',
  templateUrl: './neo.component.html',
  styleUrls: ['./neo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NeoComponent {

}
