import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NeoComponent } from './components/neo/neo.component';

const routes: Routes = [
  {
    path: '',
    component: NeoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NeoRoutingModule { }
