import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NeoRoutingModule } from './neo-routing.module';
import { SharedModule } from '@shared/shared.module';
import { NeoComponent } from './components/neo/neo.component';

@NgModule({
  declarations: [
    NeoComponent
  ],
  imports: [
    CommonModule,
    NeoRoutingModule,
    SharedModule
  ]
})
export class NeoModule { }
