import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IRouteData } from '@shared/interfaces/route-data.interface';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  /** Routes defined in root's routing module */
  protected routes: IRouteData[] = [];
  
  constructor(
    private router: Router
  ) {}
  
  ngOnInit(): void {
    // Get routes from application's root router
    this.routes = this.router.config
      .filter(route => route.data)
      .map(route => ({
        ...route.data as IRouteData,
        href: route.path ?? ''
      }));
  }
}
