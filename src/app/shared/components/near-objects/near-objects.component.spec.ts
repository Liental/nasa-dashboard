import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NearObjectsComponent } from './near-objects.component';

describe('NearObjectsComponent', () => {
  let component: NearObjectsComponent;
  let fixture: ComponentFixture<NearObjectsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NearObjectsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NearObjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
