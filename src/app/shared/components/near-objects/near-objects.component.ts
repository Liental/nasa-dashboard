import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';
import { NearEarthObjectSummary } from '@shared/dto/near-earth-objects.dto';
import { RestService } from '@shared/modules/rest/services/rest.service';
import { map, Observable, share, tap } from 'rxjs';

@Component({
  selector: 'app-near-objects',
  templateUrl: './near-objects.component.html',
  styleUrls: ['./near-objects.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NearObjectsComponent {
  /** How many objects should be displayed in the table. */
  @Input() objectsNumbers: number = 3;

  /** Did the API finish loading data */
  @Input() isReady: boolean;
  @Output() isReadyChange = new EventEmitter<boolean>();
  
  /** Response from NASA API containing nearby objects */
  protected neo$: Observable<NearEarthObjectSummary[]>;
    
  constructor (
    private restService: RestService
  ) {}

  ngOnInit(): void {
    // Can't use take() pipe because an array is not a stream
    this.neo$ = this.restService.getNeo()
      .pipe(
        share(),
        tap(res => res.nearEarthObjects.sort((a, b) => a.hazardous && !b.hazardous ? -1 : 1)),
        map(res => res.nearEarthObjects.splice(0, this.objectsNumbers)),
        tap(() => this.isReadyChange.next(true))
      );
  }

  /** Get `trackBy` of {@link apod} ngFor loop */
  protected trackBy(index: number, item: NearEarthObjectSummary) {
    return item.id;
  }
}
