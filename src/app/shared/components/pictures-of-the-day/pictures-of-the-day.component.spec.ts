import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PicturesOfTheDayComponent } from './pictures-of-the-day.component';

describe('PicturesOfTheDayComponent', () => {
  let component: PicturesOfTheDayComponent;
  let fixture: ComponentFixture<PicturesOfTheDayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PicturesOfTheDayComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PicturesOfTheDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
