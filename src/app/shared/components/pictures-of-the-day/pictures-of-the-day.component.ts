import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { PictureOfTheDayDto } from '@shared/dto/picture-of-the-day.dto';
import { RestService } from '@shared/modules/rest/services/rest.service';
import * as moment from 'moment';
import { map, Observable, share, tap } from 'rxjs';

@Component({
  selector: 'app-pictures-of-the-day',
  templateUrl: './pictures-of-the-day.component.html',
  styleUrls: ['./pictures-of-the-day.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PicturesOfTheDayComponent {
  /** How many pictures should be downloaded from the API */
  @Input() pictureLimit: number = 3;

  /** Did the API finish loading data */
  @Input() isReady: boolean;
  @Output() isReadyChange = new EventEmitter<boolean>();
  
  /** Response from NASA API containing pictures of the day */
  protected apod$: Observable<PictureOfTheDayDto[]>;
  
  constructor (
    private restService: RestService
  ) {}

  ngOnInit(): void {
    const endDate = moment()
      .startOf('day')
      .format('YYYY-MM-DD');

    const startDate = moment()
      .startOf('day')
      .subtract(this.pictureLimit - 1, 'days')
      .format('YYYY-MM-DD');

    this.apod$ = this.restService.getApod(startDate, endDate)
      .pipe(
        share(),
        map(result => result.reverse()),
        tap(() => this.isReadyChange.next(true))
      );
  }

  /** Get `trackBy` of {@link apod} ngFor loop */
  protected trackBy(index: number, item: PictureOfTheDayDto) {
    return item.date;
  }
}
