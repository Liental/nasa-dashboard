import { ChangeDetectorRef, Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { LoaderComponent } from '@shared/components/loader/loader.component';

/**
 * Used to hide elements when desired value is not set yet.
 * Displays a loader when the value is set to `true`.
 */
@Directive({ selector: '[appLoader]' })
export class LoaderDirective {
  /** Should the loader be displayed */
  @Input() set appLoader(value: any) {
    this.changeLoaderState(value);
  };
  
  constructor(
    private readonly templateRef: TemplateRef<any>,
    private readonly viewContainer: ViewContainerRef
  ) {}

  /**
   * Hide or show loader depending on the provided value
   * 
   * @param value defines whether to hide or show the loader
  */
  private changeLoaderState(value: any): void {
    this.viewContainer.clear();
    
    if (!value) {
      this.viewContainer.createComponent(LoaderComponent)
      return;
    }
    
    this.viewContainer.createEmbeddedView(this.templateRef, { appLoader: value });
  }
}
