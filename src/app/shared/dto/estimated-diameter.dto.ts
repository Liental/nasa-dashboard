import { Expose, Type } from 'class-transformer';

/** Estimated size of the object in multiple units */
export class EstimatedDiameterDto {
  @Expose()
  @Type(() => EstimatedDiameter)
  kilometers: EstimatedDiameter;

  @Expose()
  @Type(() => EstimatedDiameter)
  meters: EstimatedDiameter;

  @Expose()
  @Type(() => EstimatedDiameter)
  miles: EstimatedDiameter;
  
  @Expose()
  @Type(() => EstimatedDiameter)
  feet: EstimatedDiameter;
}

/** Estimated size of the object, same model for all units */
class EstimatedDiameter {
  @Expose({ name: 'estimated_diameter_min' }) minDiameter: number;

  @Expose({ name: 'estimated_diameter_max' }) maxDiameter: number;
}
