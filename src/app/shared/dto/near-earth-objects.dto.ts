import { Expose, plainToInstance, Transform, Type } from 'class-transformer';
import { EstimatedDiameterDto } from './estimated-diameter.dto';

export class NearEarthObjectsDto {
  /** Amount of elements in the response */
  @Expose({ name: 'element_count' }) count: number;

  /** 
   * Table containing all nearby objects in defined date range and page.
   * 
   * In API response this object is a map of `{ [date]: neo[] }`, 
   * that's why we're taking only the values.
  */
  @Expose({ name: 'near_earth_objects' })
  @Transform(options => plainToInstance(
    NearEarthObjectSummary,
    Object.values(options.value).flat(),
    { 
      excludeExtraneousValues: true,
      enableCircularCheck: true
    }
  ))
  nearEarthObjects: NearEarthObjectSummary[];
}

/** Short information about a near earth object  */
export class NearEarthObjectSummary {
  /** NASA ID for this object, can be used to fetch details */
  @Expose() id: string;

  /** Name of the object (usually an ominous number) */
  @Expose() name: string;

  /** How bright the object is */
  @Expose({ name: 'absolute_magnitude_h' }) magnitude: number;

  /** Is it possible that the object will hit anything connected to earth */
  @Expose({ name: 'is_potentially_hazardous_asteroid' }) hazardous: boolean;

  /** Estimated size of the object */
  @Expose({ name: 'estimated_diameter' })
  @Type(() => EstimatedDiameterDto)
  estimatedDiameter: EstimatedDiameterDto;
}
