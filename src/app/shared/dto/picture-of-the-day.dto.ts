import { Expose } from 'class-transformer';

export class PictureOfTheDayDto {
  /** Date of when the picture was chosen as an APOD */
  @Expose() date: string;

  /** HD resolution of the image */
  @Expose() hdurl: string;

  /** URL to the image */
  @Expose() url: string;

  /** Short image description */
  @Expose() title: string;

  /** Explanation of what is happening on the picture */
  @Expose() explanation: string;
}