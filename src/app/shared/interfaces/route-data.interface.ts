export interface IRouteData {
  icon: string;
  name: string;
  href: string;
}
