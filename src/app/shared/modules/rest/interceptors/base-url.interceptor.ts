import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { CookieService } from 'ngx-cookie-service';

/** Used to insert base URL of NASA API http web service to the URL */
@Injectable()
export class BaseUrlInterceptor implements HttpInterceptor {
  constructor(private cookieService: CookieService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const hasQuery = request.url.includes('?');
    const apiKey = this.cookieService.get('apiKey');
    
    // Modify to request to contain desired URL
    const req = request.clone({
      url: `${environment.nasaApi.baseUrl}/${request.url}${ hasQuery ? '&' : '?' }api_key=${apiKey}`
    });

    return next.handle(req);
  }
}
