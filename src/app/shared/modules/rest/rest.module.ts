import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BaseUrlInterceptor } from './interceptors/base-url.interceptor';
import { RestService } from './services/rest.service';

@NgModule({
  declarations: [],
  imports: [ HttpClientModule ],
  providers: [
    RestService,
    
    // Interceptors
    { provide: HTTP_INTERCEPTORS, useClass: BaseUrlInterceptor, multi: true }
  ]
})
export class RestModule { }
