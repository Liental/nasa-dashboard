import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { PictureOfTheDayDto } from '@shared/dto/picture-of-the-day.dto';
import { plainToInstance } from 'class-transformer';
import { NearEarthObjectsDto } from '@shared/dto/near-earth-objects.dto';

@Injectable()
export class RestService {
  constructor(private httpClient: HttpClient) { }

  /**
   * Fetch pictures of the day from last <{@link numberOfDays}> days
   * 
   * @param startDate defines the starting point of date range
   * @param endDate defines the ending date of date range (default: today)
  */
  public getApod(startDate: string, endDate?: string): Observable<PictureOfTheDayDto[]> {
    const query = new URLSearchParams();

    query.append('start_date', startDate);
    endDate && query.append('end_date', endDate);
    
    return this.httpClient
      .get<PictureOfTheDayDto[]>(`planetary/apod?${query.toString()}`)
      .pipe(
        map(value => plainToInstance(PictureOfTheDayDto, value, { excludeExtraneousValues: true }))
      );
  }

  /**
   * Fetch data about near earth objects in defined date range
   * Maxiumum number of objects in the response is 75, if you need to use more you have to use
   * {@link NearEarthObjectsDto.nextPage} table to get next page
  */
  public getNeo(): Observable<NearEarthObjectsDto> {
    return this.httpClient
    .get<PictureOfTheDayDto>(`neo/rest/v1/feed`)
    .pipe(
      map(value => plainToInstance(NearEarthObjectsDto, value, {
        excludeExtraneousValues: true,
        enableCircularCheck: true
      }))
    );
  }
}
