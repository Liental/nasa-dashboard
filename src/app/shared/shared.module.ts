import { NgModule } from '@angular/core';
import { RestModule } from './modules/rest/rest.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LoaderDirective } from './directives/loader.directive';
import { LoaderComponent } from './components/loader/loader.component';
import { NearObjectsComponent } from './components/near-objects/near-objects.component';
import { PicturesOfTheDayComponent } from './components/pictures-of-the-day/pictures-of-the-day.component';

@NgModule({
  declarations: [
    // Components
    NavbarComponent,
    NearObjectsComponent,
    PicturesOfTheDayComponent,

    // Directives
    LoaderDirective,
    LoaderComponent
  ],
  imports: [
    // Native
    RouterModule,
    CommonModule,
    
    // Custom
    RestModule,
  ],
  exports: [
    // Modules
    RestModule,

    // Components
    NavbarComponent,
    LoaderComponent,
    NearObjectsComponent,
    PicturesOfTheDayComponent,

    // Directives
    LoaderDirective
  ]
})
export class SharedModule { }
