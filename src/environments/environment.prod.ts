export const environment = {
  production: true,
  nasaApi: {
    baseUrl: 'https://api.nasa.gov',
  }
};
