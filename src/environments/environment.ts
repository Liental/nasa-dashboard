export const environment = {
  production: false,
  nasaApi: {
    baseUrl: 'https://api.nasa.gov/',
  }
};
